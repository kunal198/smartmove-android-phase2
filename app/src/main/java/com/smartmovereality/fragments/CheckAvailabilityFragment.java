package com.smartmovereality.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.smartmovereality.Activities.MainActivity;
import com.smartmovereality.AppConstant;
import com.smartmovereality.R;
import com.smartmovereality.database.SmartMoveDatabaseAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by brst-pc20 on 4/1/16.
 */
public class CheckAvailabilityFragment extends Fragment implements View.OnClickListener,RadioGroup.OnCheckedChangeListener{
   ImageView nextIV,availabilityIV,ageIV,yearIV,prevIV;
    RadioGroup availabilityRD,ageRD,labelRD,yearRD;
    RadioButton availabilityRB,ageRB,labelRB,resaleprojectRB,rentprojectRB,yearRB,readytomoveRB,underconstructionRB,age1,age2,age3,age4,age5,hotsaleRB,urgentRB,
    yearRB1,yearRB2,yearRB3,yearRB4,yearRB5,yearRB6,yearRB7,yearRB8,yearRB9;
    String availability,age,label,year;
    String object3;
    CardView ageCV,ageCVV,yearCV,yearCVV,availabilityCV;
    SmartMoveDatabaseAdapter smartMoveDatabaseAdapter;
    int exists_id;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.check_availability,container,false);
        init(view);
        return view;
    }

    public void init(View view){
        prevIV = (ImageView) view.findViewById(R.id.prevIV);
        prevIV.setOnClickListener(this);
        availabilityRD= (RadioGroup) view.findViewById(R.id.availabilityRD);
        ageRD= (RadioGroup) view.findViewById(R.id.ageRD);
        yearRD= (RadioGroup) view.findViewById(R.id.yearRD);
       // labelRD= (RadioGroup) view.findViewById(R.id.labelRD);
        readytomoveRB= (RadioButton) view.findViewById(R.id.readytomoveRB);
        underconstructionRB= (RadioButton) view.findViewById(R.id.underconstructionRB);
        age1= (RadioButton) view.findViewById(R.id.age1);
        age2= (RadioButton) view.findViewById(R.id.age2);
        age3= (RadioButton) view.findViewById(R.id.age3);
        age4= (RadioButton) view.findViewById(R.id.age4);
        age5= (RadioButton) view.findViewById(R.id.age5);
        yearRB1= (RadioButton) view.findViewById(R.id.yearRB1);
        yearRB2= (RadioButton) view.findViewById(R.id.yearRB2);
        yearRB3= (RadioButton) view.findViewById(R.id.yearRB3);
        yearRB4= (RadioButton) view.findViewById(R.id.yearRB4);
        yearRB5= (RadioButton) view.findViewById(R.id.yearRB5);
        yearRB6= (RadioButton) view.findViewById(R.id.yearRB6);
        yearRB7= (RadioButton) view.findViewById(R.id.yearRB7);
        yearRB8= (RadioButton) view.findViewById(R.id.yearRB8);
        yearRB9= (RadioButton) view.findViewById(R.id.yearRB9);
        ageCV= (CardView) view.findViewById(R.id.ageCV);
        ageCVV= (CardView) view.findViewById(R.id.ageCVV);
        yearCV= (CardView) view.findViewById(R.id.yearCV);
        availabilityCV= (CardView) view.findViewById(R.id.availabilityCV);
        availabilityIV= (ImageView) view.findViewById(R.id.availabilityIV);
        ageIV=(ImageView) view.findViewById(R.id.ageIV);
        yearIV=(ImageView) view.findViewById(R.id.yearIV);
        yearCVV= (CardView) view.findViewById(R.id.yearCVV);
      //  hotsaleRB= (RadioButton) view.findViewById(R.id.hotsaleRB);
       // urgentRB= (RadioButton) view.findViewById(R.id.urgentRB);
        availabilityRD.setOnCheckedChangeListener(this);
        smartMoveDatabaseAdapter=new SmartMoveDatabaseAdapter(getActivity());
        nextIV= (ImageView) view.findViewById(R.id.nextIV);
        availabilityIV.setOnClickListener(this);
        ageIV.setOnClickListener(this);
        yearIV.setOnClickListener(this);
        nextIV.setOnClickListener(this);
        editData();
    }


    public void onClick(View v) {
        switch (v.getId()){
            case R.id.prevIV:

                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.availabilityIV:
                if (availabilityCV.getVisibility()==View.VISIBLE){

                    availabilityCV.setVisibility(View.GONE);
                }
                else {
                    availabilityCV.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.ageIV:
                if (ageCVV.getVisibility()==View.VISIBLE){

                    ageCVV.setVisibility(View.GONE);
                }
                else {
                    ageCVV.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.yearIV:
                if (yearCVV.getVisibility()==View.VISIBLE){

                    yearCVV.setVisibility(View.GONE);
                }
                else {
                    yearCVV.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.nextIV:
                int checkid = availabilityRD.getCheckedRadioButtonId();
                availabilityRB = (RadioButton) getView().findViewById(checkid);
                int checkid1 = ageRD.getCheckedRadioButtonId();
                ageRB = (RadioButton) getView().findViewById(checkid1);
               /* int checkid2 = labelRD.getCheckedRadioButtonId();
                labelRB = (RadioButton) getView().findViewById(checkid2);*/
                int checkid3 = yearRD.getCheckedRadioButtonId();
                yearRB = (RadioButton) getView().findViewById(checkid3);
                if (availabilityRB!=null)
                availability=availabilityRB.getText().toString();
                if (ageRB!=null)
                age=ageRB.getText().toString();
               // if (labelRB!=null)
                //label=labelRB.getText().toString();
                if (yearRB!=null)
                    year=yearRB.getText().toString();
                String data=dataInJson();
                if (exists_id>99) {
                    smartMoveDatabaseAdapter.updateData(AppConstant.KEY_CHECK_AVAILABILITY,data,exists_id);
                }
                else {
                    smartMoveDatabaseAdapter.updateData(AppConstant.KEY_CHECK_AVAILABILITY,data,AppConstant.UNIQUE_ID);
                }
                if (exists_id>99){
                    Bundle bundle=new Bundle();
                    bundle.putInt("fwd_id", exists_id);
                    PropertyTitleFragment propertyTitleFragment = new PropertyTitleFragment();
                    propertyTitleFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction().
                            replace(R.id.container, propertyTitleFragment).addToBackStack(null).commit();
                }
                else {
                    PropertyTitleFragment propertyTitleFragment = new PropertyTitleFragment();
                    getFragmentManager().beginTransaction().
                            replace(R.id.container, propertyTitleFragment).addToBackStack(null).commit();

                }

               // smartMoveDatabaseAdapter.updateData(AppConstant.KEY_CHECK_AVAILABILITY, data,AppConstant.UNIQUE_ID);

                break;
        }
    }
    public void editData(){
        String str_editable_data;
        Bundle bundle=getArguments();
        if (bundle!=null)
        {
            exists_id=   bundle.getInt("fwd_id");
        }
        if (exists_id!=0)
        {
            str_editable_data=  smartMoveDatabaseAdapter.getSelectItems(exists_id, AppConstant.KEY_CHECK_AVAILABILITY);
            Log.d("str_editable_data",""+str_editable_data);
            try {
                JSONObject jsonObject = new JSONObject(str_editable_data);
                JSONArray jsonArray = jsonObject.optJSONArray("jsonobject");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                    String label=jsonObject1.optString("label");
                    String age=jsonObject1.optString("age");
                    String year=jsonObject1.optString("year");
                    String availability=jsonObject1.optString("availability");
                    if (readytomoveRB.getText().toString().equals(availability))
                    readytomoveRB.setChecked(true);
                    if (underconstructionRB.getText().toString().equals(availability))
                        underconstructionRB.setChecked(true);
                    if (age1.getText().toString().equals(age))
                        age1.setChecked(true);
                    if (age2.getText().toString().equals(age))
                        age2.setChecked(true);
                    if (age3.getText().toString().equals(age))
                        age3.setChecked(true);
                    if (age4.getText().toString().equals(age))
                        age4.setChecked(true);
                    if (age5.getText().toString().equals(age))
                        age5.setChecked(true);
                    if (yearRB1.getText().toString().equals(year))
                        yearRB1.setChecked(true);
                    if (yearRB2.getText().toString().equals(year))
                        yearRB2.setChecked(true);

                    if (yearRB2.getText().toString().equals(year))
                        yearRB2.setChecked(true);
                    if (yearRB3.getText().toString().equals(year))
                        yearRB3.setChecked(true);
                    if (yearRB4.getText().toString().equals(year))
                        yearRB4.setChecked(true);

                    if (yearRB5.getText().toString().equals(year))
                        yearRB5.setChecked(true);
                    if (yearRB6.getText().toString().equals(year))
                        yearRB6.setChecked(true);
                    if (yearRB7.getText().toString().equals(year))
                        yearRB7.setChecked(true);

                    if (yearRB8.getText().toString().equals(year))
                        yearRB8.setChecked(true);

                }
            }catch(JSONException e){
                e.printStackTrace();
            }
        }
    }

    public String dataInJson(){
        JSONObject jsonObject=new JSONObject();
        JSONObject jsonObject1=new JSONObject();
        JSONArray jsonArray=new JSONArray();

        try {
            jsonObject1.put("availability",availability);
            jsonObject1.put("age", age);
            jsonObject1.put("label",label);
            jsonObject1.put("year",year);
            jsonArray.put(jsonObject1);
            jsonObject.put("jsonobject",jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("oboooooo",""+jsonObject);
        object3=jsonObject.toString();
        Log.d("jso", "" + jsonObject);
        return object3;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
       if (checkedId==underconstructionRB.getId()){
            ageCV.setVisibility(View.GONE);
           ageCVV.setVisibility(View.GONE);
           yearCV.setVisibility(View.VISIBLE);
           yearCVV.setVisibility(View.VISIBLE);
        }
        else {
            ageCV.setVisibility(View.VISIBLE);
           yearCV.setVisibility(View.GONE);
           ageCVV.setVisibility(View.VISIBLE);
           yearCVV.setVisibility(View.GONE);

       }
    }
}
