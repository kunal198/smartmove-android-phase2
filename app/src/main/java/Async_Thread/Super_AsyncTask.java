package Async_Thread;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.HashMap;


/**
 Created by sharan on 10/9/15. */
public class Super_AsyncTask extends AsyncTask<Void, Void, String>
{

    String URL;
    HashMap<String, String> inputData = null;
    Context        con;
    ProgressDialog dialog;
    Super_AsyncTask_Interface listener                = null;
    boolean                   show_progressbar_or_not = false;
   // MyConstants myConstants                  = new MyConstants();

    public Super_AsyncTask(Context con, HashMap<String, String> inputData, String URL, Super_AsyncTask_Interface listener, boolean show_progressbar_or_not)
    {
        this.con = con;
        this.inputData = inputData;
        this.URL = URL;
        this.listener = listener;
        this.show_progressbar_or_not = show_progressbar_or_not;

     //   myConstants.hide_keyboard(con);
    }

    public Super_AsyncTask(Context con, String URL, Super_AsyncTask_Interface listener, boolean show_progressbar_or_not)
    {
        this.con = con;
        this.URL = URL;
        this.listener = listener;
        this.show_progressbar_or_not = show_progressbar_or_not;
       // myConstants.hide_keyboard(con);
    }


  /*  public void cancelAsync()
    {
        if (super_asyncTask!=null)
        {
            super_asyncTask.cancel(true);
        }

    }*/

    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params)
    {
        String response = "";

        Log.e("inputData", "" + inputData);
        try
        {
            if (inputData != null)
            {
                response = new WebServiceHandler().performPostCall(URL, inputData);
            }
            else
            {
                response = new WebServiceHandler().performGetCall(URL);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPostExecute(String ResponseString)
    {
        super.onPostExecute(ResponseString);

        Log.e("Response for " + con.getClass().getName(), " " + ResponseString);

        if (!ResponseString.equals("SLOW") && !ResponseString.equals("ERROR"))
        {
            listener.onTaskCompleted(ResponseString);

        }
        else if (ResponseString.equals("SLOW"))
        {

        }
        else if (ResponseString.equals("ERROR"))
        {

        }

    }
}
