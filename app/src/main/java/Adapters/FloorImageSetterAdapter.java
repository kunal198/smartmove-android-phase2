package Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.ArrayAdapter;
import com.smartmovereality.R;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 4/19/16.
 */
public class FloorImageSetterAdapter extends ArrayAdapter<String> {
    Context context;
    ArrayList<Bitmap> floor_gallery;


    public FloorImageSetterAdapter(Context context, ArrayList floor_gallery) {
      super(context,R.layout.floor_plan_single_row,R.id.floorIVV,floor_gallery);
        this.context = context;
        this.floor_gallery = floor_gallery;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        viewHolder mhHolder = null;
        if (view == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.floor_plan_single_row, parent, false);
            mhHolder = new viewHolder(view);
            view.setTag(mhHolder);
        } else {
            mhHolder = (viewHolder) view.getTag();
        }


        Bitmap bitmap=floor_gallery.get(position);
        mhHolder.floorIVV.setImageBitmap(bitmap);


        return view;
    }


    private class viewHolder {
        ImageView floorIVV;

        viewHolder(View view) {

            floorIVV = (ImageView) view.findViewById(R.id.floorIVV);

        }

    }
}


