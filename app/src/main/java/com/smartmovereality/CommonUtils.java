package com.smartmovereality;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;


import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.L;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;

import Async_Thread.Super_AsyncTask;

public class CommonUtils {
    public Context context;
    String sdd = "";
    public static ProgressDialog mprogress;
    String d = null;
    SharedPreferences sharedPreferences;
    ArrayList<String> property_category_commercial_value = new ArrayList<String>();
    ArrayList<String> property_category_commercial_key = new ArrayList<String>();
    ArrayList<String> property_category_residential_value = new ArrayList<String>();
    ArrayList<String> property_category_residential_key = new ArrayList<String>();
    ArrayList<String> property_types_commercial_value = new ArrayList<String>();
    ArrayList<String> property_types_commercial_key = new ArrayList<String>();

    public static String URL = "http://smartmoverealty.in/?";

    public CommonUtils(Context context) {
        this.context = context;

    }


    public static void dialog(Context context, String msg) {
        mprogress = new ProgressDialog(context);
        mprogress.setMessage(msg);
        mprogress.show();
    }

    public void alertMessage(String msg, String title) {
        AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(context);
        alertdialogbuilder.setTitle(title);
        alertdialogbuilder.setMessage(msg);
        alertdialogbuilder.setCancelable(false);
        alertdialogbuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertdialogbuilder.create();
        alertDialog.show();
    }

    public static boolean isValidInputs(String user, String pass) {
        if (!user.isEmpty() || !pass.isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean isValidInputs(String user) {
        if (!user.isEmpty()) {
            return true;
        }
        return false;
    }


    public boolean isNetworkConnectionAvailable() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if ((connectivityManager.getActiveNetworkInfo().getType()) == (ConnectivityManager.TYPE_WIFI)
                    || (connectivityManager.getActiveNetworkInfo().getType()) == (ConnectivityManager.TYPE_MOBILE)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setUserLogin(String id) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("userloginid", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        int ids = Integer.parseInt(id);
        editor.putInt("id", ids);
        editor.commit();


    }

    public int getUserLogin() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("userloginid", Context.MODE_PRIVATE);
        int id = sharedPreferences.getInt("id", -1);
        return id;
    }

    public void storeData(String[] key, String[] value) {
        sharedPreferences = context.getSharedPreferences("store_data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        for (int i = 0; i < key.length; i++) {
            editor.putString(key[i], value[i]);

        }
        editor.commit();
    }

    /*public void storeCheckboxData(ArrayList<ArrayList<String>> checkBoxSelection) {
        sharedPreferences = context.getSharedPreferences("store_data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        for (int i = 0; i < checkBoxSelection.size(); i++) {
            if (i == 0)
                property_category_commercial_key = checkBoxSelection.get(i);
            if (i == 1)
                property_category_commercial_value = checkBoxSelection.get(i);
            if (i == 2)
                property_category_residential_key = checkBoxSelection.get(i);
            if (i == 3)
                property_category_residential_value = checkBoxSelection.get(i);
            if (i == 4)
                property_types_commercial_key = checkBoxSelection.get(i);
            if (i == 5)
                property_types_commercial_value = checkBoxSelection.get(i);

        }
        Gson gson = new Gson();

        String json1 = gson.toJson(property_category_commercial_key);
        String json2 = gson.toJson(property_category_commercial_value);
        String json3 = gson.toJson(property_category_residential_key);
        String json4 = gson.toJson(property_category_residential_value);
        String json5 = gson.toJson(property_types_commercial_key);
        String json6 = gson.toJson(property_types_commercial_value);
        editor.putString(json1, json2);
        editor.putString(json3, json4);
        editor.putString(json5, json6);
        editor.commit();


    }*/
    public Bitmap ShrinkBitmap(String file, int width, int height) {
        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

        int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight / (float) height);
        int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth / (float) width);

        if (heightRatio > 1 || widthRatio > 1) {
            if (heightRatio > widthRatio) {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }

    public void setUniqueId(int unique_id) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("uniqueid", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("unique_id", unique_id);
        editor.commit();

    }

    public int getUniqueId() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("uniqueid", Context.MODE_PRIVATE);
        int id = sharedPreferences.getInt("unique_id", -1);
        return id;
    }


    /*

        public void getCheckboxData(){
             sharedPreferences =  context.getSharedPreferences("store_data", Context.MODE_PRIVATE);
            Gson gson = new Gson();
            String json1 = sharedPreferences.getString("json1", null);
            String json2 = sharedPreferences.getString("json2", null);
            String json3= sharedPreferences.getString("json3", null);

            Type type = new TypeToken<ArrayList<ArrayObject>>() {}.getType();
            ArrayList<String> arrayList1 = gson.fromJson(json1, type);
            ArrayList<String> arrayList2 = gson.fromJson(json2, type);
            ArrayList<String> arrayList3 = gson.fromJson(json3, type);
            Log.d("araylist",""+arrayList1);
            for (int i=0;i<arrayList1.size();i++)
            {

            }
    */
    public void alertForDropdown(final String arr[], final TextView textView)

    {
        AlertDialog.Builder alertbuilder = new AlertDialog.Builder(context);
        alertbuilder.setTitle("Make your selection");
        alertbuilder.setItems(arr, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                //  mDoneButton.setText(items[item]);
                Log.e("items[item]", "" + arr[item]);
                String selectedvalue = arr[item];
                textView.setText(selectedvalue);
            }
        });
        AlertDialog alert = alertbuilder.create();
        alert.show();

    }
    public static void execute(Super_AsyncTask asyncTask)
    {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else
        {
            asyncTask.execute();
        }

    }
    public void set_memory_Storage(Boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("uniqueid", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("value", value);
        editor.commit();

    }

    public Boolean get_memory() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("uniqueid", Context.MODE_PRIVATE);
        Boolean value = sharedPreferences.getBoolean("value", false);
        return value;
    }

    public static void init(Context app) {
        File cacheDir = StorageUtils.getCacheDirectory(app);
        ImageLoader imageLoader = ImageLoader.getInstance();
        // L.writeDebugLogs(false);
        //  L.writeLogs(false);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(app).discCache(new UnlimitedDiscCache(cacheDir)).denyCacheImageMultipleSizesInMemory().
                discCacheFileNameGenerator(new HashCodeFileNameGenerator()).tasksProcessingOrder(QueueProcessingType.LIFO).build();

        if (!imageLoader.isInited())
            imageLoader.init(config);
    }


}