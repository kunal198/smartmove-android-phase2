package com.smartmovereality.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.smartmovereality.AppConstant;
import com.smartmovereality.AppController;
import com.smartmovereality.R;
import com.smartmovereality.CommonUtils;

import org.json.JSONObject;

import Async_Thread.Super_AsyncTask;
import Async_Thread.Super_AsyncTask_Interface;

/**
 * Created by brst-pc20 on 4/5/16.
 */
public class LoginActivity extends Activity implements View.OnClickListener {
    Button loginBT;
    EditText edituserTV, editpassTV;

    CommonUtils commonUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        commonUtils = new CommonUtils(this);
        init();
    }

    public void init() {
        loginBT = (Button) findViewById(R.id.loginBT);
        edituserTV = (EditText) findViewById(R.id.edituserTV);
        editpassTV = (EditText) findViewById(R.id.editpassTV);
        loginBT.setOnClickListener(this);

    }


    public void getData(String username, String password) {

        String LOGIN_URL = AppConstant.URL + "u_name=" + username + "&u_pass=" + password;
       uploadDataToServer(LOGIN_URL);





    }
    public void uploadDataToServer(final String url) {


        commonUtils.execute(new Super_AsyncTask(this, url, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {

                try {
                    JSONObject jsonObject=new JSONObject(output);
                    String id=jsonObject.optString("id");
                    Log.d("outputresponse", "" + output);
                    Intent intent = new Intent(LoginActivity.this, PushToLiveActivity.class);
                    startActivity(intent);
                    finish();
                    commonUtils.setUserLogin(id);


                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }

            }
        }, true));
    }


    @Override
    public void onClick(View v) {
        String user_name = edituserTV.getText().toString();
        String password = editpassTV.getText().toString();

            if (commonUtils.isValidInputs(user_name, password)) {
                if (commonUtils.isValidInputs(user_name)) {
                    if (commonUtils.isValidInputs(password)) {
                        getData(user_name, password);
                    } else {
                        commonUtils.alertMessage(AppConstant.PASSWORD, "");
                    }

                } else {

                    commonUtils.alertMessage(AppConstant.USER_NAME, "");

                }
            } else {
                commonUtils.alertMessage(AppConstant.ALERT_MESSAGE, "");

            }


    }
}