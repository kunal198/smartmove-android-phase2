package com.smartmovereality.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.smartmovereality.Activities.MainActivity;
import com.smartmovereality.AppConstant;
import com.smartmovereality.CommonUtils;
import com.smartmovereality.R;
import com.smartmovereality.database.SmartMoveDatabaseAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 4/2/16.
 */
public class PropertyTitleFragment extends Fragment implements View.OnClickListener {
    ImageView nextIV,locationIV,slugIV,discussionIV,descriptionIV,titleIV,prevIV;
    EditText titleET, descriptionET, slugET, cityET, addressET, zipET, neighbourET;
    CheckBox allowcommentCB,allowtrackbackCB;
    SmartMoveDatabaseAdapter smartMoveDatabaseAdapter;
    int exists_id;
    CardView titleCV,descriptionCV,discussionCV,slugCV,locationCV;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.property_title, container, false);
        init(view);
        return view;
    }

    public void init(View view) {
        prevIV = (ImageView) view.findViewById(R.id.prevIV);
        prevIV.setOnClickListener(this);
        titleIV= (ImageView) view.findViewById(R.id.titleIV);
        descriptionIV= (ImageView) view.findViewById(R.id.descriptionIV);
        discussionIV = (ImageView) view.findViewById(R.id.discussionIV);
        slugIV = (ImageView) view.findViewById(R.id.slugIV);
        locationIV = (ImageView) view.findViewById(R.id.locationIV);
        nextIV = (ImageView) view.findViewById(R.id.nextIV);
        titleET = (EditText) view.findViewById(R.id.titleET);
        descriptionET = (EditText) view.findViewById(R.id.descriptionET);
        slugET = (EditText) view.findViewById(R.id.slugET);
        cityET = (EditText) view.findViewById(R.id.cityET);
        addressET = (EditText) view.findViewById(R.id.addressET);
        zipET = (EditText) view.findViewById(R.id.zipET);
        neighbourET = (EditText) view.findViewById(R.id.neighbourET);
        allowcommentCB = (CheckBox) view.findViewById(R.id.allowcommentCB);
        allowtrackbackCB = (CheckBox) view.findViewById(R.id.allowtrackbackCB);
        titleCV= (CardView) view.findViewById(R.id.titleCV);
        descriptionCV= (CardView) view.findViewById(R.id.descriptionCV);
        discussionCV= (CardView) view.findViewById(R.id.discussionCV);
        slugCV= (CardView) view.findViewById(R.id.slugCV);
        locationCV= (CardView) view.findViewById(R.id.locationCV);
        smartMoveDatabaseAdapter=new SmartMoveDatabaseAdapter(getActivity());
        nextIV.setOnClickListener(this);
        titleIV.setOnClickListener(this);
        descriptionIV.setOnClickListener(this);
        discussionIV.setOnClickListener(this);
        slugIV.setOnClickListener(this);
        locationIV.setOnClickListener(this);

        editData();

        descriptionET.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() == R.id.descriptionET) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

    }

    public String getDataToJson() {
        ArrayList<JSONObject> arrayList = new ArrayList<>();
        String title = titleET.getText().toString();
        String description = descriptionET.getText().toString();
        String slug = slugET.getText().toString();
        String city = cityET.getText().toString();
        String address = addressET.getText().toString();
        String allow_comment = allowcommentCB.getText().toString();
        String allow_track = allowtrackbackCB.getText().toString();
        String zip = zipET.getText().toString();
        String neighbour = neighbourET.getText().toString();

        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();

        JSONArray jsonArray=new JSONArray();
        try {
            if (!title.isEmpty()) {
                jsonObject1.put("title", title);

                //arrayList.add(jsonObject1);
            }
            if (!description.isEmpty()) {
                jsonObject1.put("description", description);

                //arrayList.add(jsonObject2);
            }
            if (!slug.isEmpty()) {
                jsonObject1.put("slug", slug);

                //arrayList.add(jsonObject3);
            }
            if (!city.isEmpty()) {
                jsonObject1.put("city", city);

               // arrayList.add(jsonObject4);
            }

            if (!address.isEmpty()) {
                jsonObject1.put("address", address);


            }
            if (!zip.isEmpty()) {
                jsonObject1.put("zip", zip);


            }
            if (!neighbour.isEmpty()) {
                jsonObject1.put("neighbour", neighbour);


            }
            if (!allow_comment.isEmpty()) {
                jsonObject1.put("allow_comment", allow_comment);

            }
            if (!allow_track.isEmpty()) {
                jsonObject1.put("allow_track", allow_track);

            }
            jsonArray.put(jsonObject1);

            if (!jsonArray.toString().isEmpty())
           jsonObject.put("jsonarray",jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("obj", "" + jsonObject);
        String object4=jsonObject.toString();
        Log.d("objobject4", "" + object4);
        return object4;

    }
    public void editData(){
        String str_editable_data;
        Bundle bundle=getArguments();
        if (bundle!=null)
        {
            exists_id=   bundle.getInt("fwd_id");
        }
        if (exists_id!=0)
        {
            str_editable_data=  smartMoveDatabaseAdapter.getSelectItems(exists_id, AppConstant.KEY_PROPERTY_AREA);
            Log.d("str_editable_data",""+str_editable_data);
            try {
                JSONObject jsonObject = new JSONObject(str_editable_data);
                JSONArray jsonArray = jsonObject.optJSONArray("jsonarray");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                    String zip=jsonObject1.optString("zip");
                    String title=jsonObject1.optString("title");
                    String address=jsonObject1.optString("address");
                    String description=jsonObject1.optString("description");
                    String allow_track=jsonObject1.optString("allow_track");
                    String allow_comment=jsonObject1.optString("allow_comment");
                    String slug=jsonObject1.optString("slug");
                    String neighbour=jsonObject1.optString("neighbour");
                    String city=jsonObject1.optString("city");
                    if (!zip.isEmpty())
                        zipET.setText(zip);
                    if (!title.isEmpty())
                        titleET.setText(title);
                    if (!address.isEmpty())
                        addressET.setText(address);
                    if (!description.isEmpty())
                        descriptionET.setText(description);
                    if (!allow_track.isEmpty())
                        allowtrackbackCB.setChecked(true);
                    if (!allow_comment.isEmpty())
                        allowcommentCB.setChecked(true);
                    if (!slug.isEmpty())
                        slugET.setText(slug);
                    if (!neighbour.isEmpty())
                        neighbourET.setText(neighbour);
                    if (!city.isEmpty())
                        cityET.setText(city);

                }
            }catch(JSONException e){
                e.printStackTrace();
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevIV:

                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.locationIV:

                if (locationCV.getVisibility()==View.VISIBLE){

                    locationCV.setVisibility(View.GONE);
                }
                else {
                    locationCV.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.slugIV:
                if (slugCV.getVisibility()==View.VISIBLE){

                    slugCV.setVisibility(View.GONE);
                }
                else {
                    slugCV.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.discussionIV:

                if (discussionCV.getVisibility()==View.VISIBLE){

                    discussionCV.setVisibility(View.GONE);
                }
                else {
                    discussionCV.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.descriptionIV:

                if (descriptionCV.getVisibility()==View.VISIBLE){

                    descriptionCV.setVisibility(View.GONE);
                }
                else {
                    descriptionCV.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.titleIV:

                if (titleCV.getVisibility()==View.VISIBLE){

                    titleCV.setVisibility(View.GONE);
                }
                else {
                    titleCV.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.backIV:

                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.nextIV:

                String data=getDataToJson();

                if (exists_id>99) {
                    smartMoveDatabaseAdapter.updateData(AppConstant.KEY_PROPERTY_AREA,data,exists_id);
                }
                else {
                    smartMoveDatabaseAdapter.updateData(AppConstant.KEY_PROPERTY_AREA,data,AppConstant.UNIQUE_ID);
                }
                if (exists_id>99){
                    Bundle bundle=new Bundle();
                    bundle.putInt("fwd_id", exists_id);
                    AdditionalInformationFragment additionalInformationFragment=new AdditionalInformationFragment();
                    additionalInformationFragment.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, additionalInformationFragment).addToBackStack(null);
                    fragmentTransaction.commit();
                }
                else {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, new AdditionalInformationFragment()).addToBackStack(null);
                    fragmentTransaction.commit();

                }
                break;
        }


    }
}