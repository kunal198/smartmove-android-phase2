package com.smartmovereality;

import android.graphics.Bitmap;
import android.support.v7.widget.AppCompatImageView;

import java.io.Serializable;

/**
 * Created by brst-pc20 on 4/13/16.
 */
public class Data implements Serializable {

    private String title;
    private String city;
    private String neighbour;
    private String status;
    private String area;
    private String highlights;
    private String price;
    private Bitmap gallery_image;
private Bitmap gallery_grid;


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNeighbour() {
        return neighbour;
    }

    public void setNeighbour(String neighbour) {
        this.neighbour = neighbour;
    }

    public Bitmap getGallery_grid() {
        return gallery_grid;
    }

    public void setGallery_grid(Bitmap gallery_grid) {
        this.gallery_grid = gallery_grid;
    }

    public String getPrice() {
        return price;
    }




    public Bitmap getGallery_image() {
        return gallery_image;
    }

    public void setGallery_image(Bitmap gallery_image) {
        this.gallery_image = gallery_image;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTitle() {
        return title;


    }

    public String getHighlights() {
        return highlights;
    }

    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
