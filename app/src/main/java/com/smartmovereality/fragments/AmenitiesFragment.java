package com.smartmovereality.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.pm.PackageManager;
import android.drm.DrmStore;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.GridView;
import android.widget.ViewSwitcher;

import com.android.volley.toolbox.ImageLoader;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.smartmovereality.Activities.Action;
import com.smartmovereality.Activities.MainActivity;
import com.smartmovereality.Activities.PushToLiveActivity;
import com.smartmovereality.AppConstant;
import com.smartmovereality.CommonUtils;
import com.smartmovereality.Data;
import com.smartmovereality.R;
import com.smartmovereality.database.SmartMoveDatabaseAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import Adapters.CustomGallery;
import Adapters.FloorImageSetterAdapter;
import Adapters.GalleryAdapter;
import Adapters.GalleryAdapterGallery;
import Adapters.ImageSetterAdapter;

/**
 * Created by brst-pc20 on 4/2/16.
 */
public class AmenitiesFragment extends Fragment implements View.OnClickListener {
    Button submitBT, floorplanBT, photogalleryBT;
    ImageView imageIV, amenityIV, floorplanIV, agentIV, videoIV, photogalleryIV,prevIV;
    CheckBox clubhouseCB, daycareCB, multipurposehallCB, sportsCB, videosecurityCB, kidsplayCB,
            swimmingpoolCB, gymCB, restaurentCB, landscapeCB;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 101;

    String picturePath;
    ExpandableHeightGridView galleryGV, floorGV;
    Uri selectedImage;
    File file = null;
    Bitmap bitmap = null;
    private static int RESULT_OK = -1;
    File destination;
    int j;
    AlertDialog alert;
    CommonUtils commonUtils;
    JSONObject data_object = new JSONObject();
    final String[] items1 = {
            "agent123", "Miss Nidhi Sharma", "SMART MOVE REALTY", "Mr. Vikas Sharma"};
    TextView agentET, videoeditTV;
    EditText vidsourceET;
    static int count = 1;
    SmartMoveDatabaseAdapter smartMoveDatabaseAdapter;
    JSONArray store_images_floor_plans = new JSONArray();
    ArrayList<Bitmap> get_images_gallery = new ArrayList<>();
    ArrayList<Bitmap> store_images_gallery = new ArrayList<>();
    JSONArray gallery_json_object = new JSONArray();
    JSONArray floor_json_object = new JSONArray();
    int exists_id;
    JSONArray jsonArray1;
    JSONArray jsonArray2;

    GalleryAdapter adapter;
    GalleryAdapterGallery adapter1;

    com.nostra13.universalimageloader.core.ImageLoader imageLoader;

    ArrayList<CustomGallery> camera_picture;


    ArrayList<CustomGallery> imagesArray;

    ArrayList<CustomGallery> floor_images_array_list;
    CardView amenitiesCV, floorCV, agentCV, videoCV, galleryCV;

    public String getDataInJson() {

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject2 = new JSONObject();

        try {
            if (clubhouseCB.isChecked()) {
                data_object.put("clubhouseCB", (clubhouseCB.getText().toString()));
            }
            if (daycareCB.isChecked()) {
                data_object.put("daycareCB", (daycareCB.getText().toString()));
            }

            if (multipurposehallCB.isChecked()) {

                data_object.put("multipurposehallCB", (multipurposehallCB.getText().toString()));
            }

            if (sportsCB.isChecked()) {
                data_object.put("sportsCB", (sportsCB.getText().toString()));
            }
            if (videosecurityCB.isChecked()) {
                data_object.put("videosecurityCB", (videosecurityCB.getText().toString()));
            }
            if (kidsplayCB.isChecked()) {
                data_object.put("kidsplayCB", (kidsplayCB.getText().toString()));
            }
            if (swimmingpoolCB.isChecked()) {
                data_object.put("swimmingpoolCB", (swimmingpoolCB.getText().toString()));
            }
            if (gymCB.isChecked()) {
                data_object.put("gymCB", (gymCB.getText().toString()));
            }
            if (restaurentCB.isChecked()) {
                data_object.put("restaurentCB", (restaurentCB.getText().toString()));
            }
            if (landscapeCB.isChecked()) {
                data_object.put("landscapeCB", (landscapeCB.getText().toString()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        jsonArray.put(data_object);


        try {
            jsonObject.put("data", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!agentET.getText().toString().isEmpty()) {

            try {
                jsonObject2.put("Agent", agentET.getText().toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!videoeditTV.getText().toString().isEmpty()) {

            try {
                jsonObject2.put("Videoedit", videoeditTV.getText().toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (!vidsourceET.getText().toString().isEmpty()) {

            try {
                jsonObject2.put("vidsource", vidsourceET.getText().toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            jsonObject2.put("gallery_json_object", gallery_json_object);
            jsonObject2.put("floor_json_object", floor_json_object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("video_source_data", jsonObject2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String jsondata = jsonObject.toString();
        return jsondata;

    }

    private void initImageLoader() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
        ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
                getActivity()).defaultDisplayImageOptions(defaultOptions).memoryCache(
                new WeakMemoryCache());

        ImageLoaderConfiguration config = builder.build();
        imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        imageLoader.init(config);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.amenities, container, false);
        initImageLoader();
        init(view);
        editData();
        return view;
    }

    public void init(View view) {
        smartMoveDatabaseAdapter = new SmartMoveDatabaseAdapter(getActivity());
        commonUtils = new CommonUtils(getActivity());
        prevIV = (ImageView) view.findViewById(R.id.prevIV);
        prevIV.setOnClickListener(this);
        submitBT = (Button) view.findViewById(R.id.submitBT);
        floorplanBT = (Button) view.findViewById(R.id.floorplanBT);
        photogalleryBT = (Button) view.findViewById(R.id.photogalleryBT);
        imageIV = (ImageView) view.findViewById(R.id.imageIV);
        galleryGV = (ExpandableHeightGridView) view.findViewById(R.id.galleryGV);
        floorGV = (ExpandableHeightGridView) view.findViewById(R.id.floorGV);
        agentET = (TextView) view.findViewById(R.id.agentET);
        videoeditTV = (TextView) view.findViewById(R.id.videoeditTV);
        vidsourceET = (EditText) view.findViewById(R.id.vidsourceET);
        clubhouseCB = (CheckBox) view.findViewById(R.id.clubhouseCB);
        daycareCB = (CheckBox) view.findViewById(R.id.daycareCB);
        multipurposehallCB = (CheckBox) view.findViewById(R.id.multipurposehallCB);
        sportsCB = (CheckBox) view.findViewById(R.id.sportsCB);
        videosecurityCB = (CheckBox) view.findViewById(R.id.videosecurityCB);
        kidsplayCB = (CheckBox) view.findViewById(R.id.kidsplayCB);
        swimmingpoolCB = (CheckBox) view.findViewById(R.id.swimmingpoolCB);
        gymCB = (CheckBox) view.findViewById(R.id.gymCB);
        restaurentCB = (CheckBox) view.findViewById(R.id.restaurentCB);
        landscapeCB = (CheckBox) view.findViewById(R.id.landscapeCB);
        amenityIV = (ImageView) view.findViewById(R.id.amenityIV);
        floorplanIV = (ImageView) view.findViewById(R.id.floorplanIV);
        agentIV = (ImageView) view.findViewById(R.id.agentIV);
        videoIV = (ImageView) view.findViewById(R.id.videoIV);
        photogalleryIV = (ImageView) view.findViewById(R.id.photogalleryIV);
        amenitiesCV = (CardView) view.findViewById(R.id.amenitiesCV);
        floorCV = (CardView) view.findViewById(R.id.floorCV);
        agentCV = (CardView) view.findViewById(R.id.agentCV);
        videoCV = (CardView) view.findViewById(R.id.videoCV);
        galleryCV = (CardView) view.findViewById(R.id.galleryCV);
        galleryGV.setFastScrollEnabled(true);
        adapter = new GalleryAdapter(getActivity(), imageLoader);
        adapter1 = new GalleryAdapterGallery(getActivity(), imageLoader);
        galleryGV.setAdapter(adapter1);
        submitBT.setOnClickListener(this);
        floorplanBT.setOnClickListener(this);
        photogalleryBT.setOnClickListener(this);
        agentET.setOnClickListener(this);
        vidsourceET.setOnClickListener(this);
        videoeditTV.setOnClickListener(this);
        amenityIV.setOnClickListener(this);
        floorplanIV.setOnClickListener(this);
        agentIV.setOnClickListener(this);
        videoIV.setOnClickListener(this);
        photogalleryIV.setOnClickListener(this);
        checkPermissions();

        Bundle bundle = getArguments();
        if (bundle != null) {
            exists_id = bundle.getInt("fwd_id");
        }

        imagesArray = new ArrayList<>();
        floor_images_array_list = new ArrayList<>();

        floorGV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                //data.remove(arg2);
                Toast.makeText(getActivity(), "sdfs "+arg2, Toast.LENGTH_SHORT).show();
                // adapter.notifyDataSetChanged();
                //adapter.notifyDataSetInvalidated();
                return true;
            }
        });
        floorGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "sdfs 11", Toast.LENGTH_SHORT).show();

            }
        });
    }


    public void editData() {
        String gym = "", kidsplay = "", swimmingpool = "", multipurposehall = "", restaurent = "", daycare = "", sports = "",
                landscape = "", videosecurity = "", clubhouse = "";
        String str_editable_data;

        Log.d("inside_resume", "jghhgjgfhjgh");
        CommonUtils.init(getActivity());
        if (exists_id != 0) {
            str_editable_data = smartMoveDatabaseAdapter.getSelectItems(exists_id, AppConstant.KEY_AMENITIES);
            Log.d("str_editable_data", "" + str_editable_data);
            try {
                JSONObject jsonObject = new JSONObject(str_editable_data);
                Log.d("str_editable_data1111", "" + str_editable_data);
                JSONObject jsonObject1 = jsonObject.optJSONObject("video_source_data");
                Log.d("video_source_data1111", "" + jsonObject1);
                String Agent = jsonObject1.optString("Agent");
                String vidsource = jsonObject1.optString("vidsource");
                String Videoedit = jsonObject1.optString("Videoedit");
                jsonArray2 = jsonObject1.optJSONArray("floor_json_object");

                for (int i = 0; i < jsonArray2.length(); i++) {
                    CustomGallery item = new CustomGallery();
                    floor_json_object.put(jsonArray2.optString(i));
                    item.sdcardPath = jsonArray2.optString(i);
                    Log.d("floor_json_object11", "" + jsonArray2.toString());

                    floor_images_array_list.add(item);

                }


                Log.d("floor_json_object11", "" + floor_json_object);
                Log.d("floor_images_array_list", "" + floor_images_array_list.toString());
                // adapter = new GalleryAdapter(getActivity(), imageLoader);
                adapter.addAll(floor_images_array_list);
                floorGV.setAdapter(adapter);
                jsonArray1 = jsonObject1.optJSONArray("gallery_json_object");
                for (int i = 0; i < jsonArray1.length(); i++) {
                    CustomGallery item = new CustomGallery();
                    gallery_json_object.put(jsonArray1.optString(i));
                    item.sdcardPath = jsonArray1.optString(i);
                    Log.d("floor_json_object11", "" + jsonArray1.toString());
                    //  get_images_gallery.add(bitmap);
                    imagesArray.add(item);

                }
                adapter1.addAll(imagesArray);
                galleryGV.setAdapter(adapter1);


                JSONArray jsonArray2 = jsonObject.optJSONArray("data");
                for (int k = 0; k < jsonArray2.length(); k++) {
                    JSONObject jsonObject2 = jsonArray2.getJSONObject(k);
                    gym = jsonObject2.optString("gymCB");
                    kidsplay = jsonObject2.optString("kidsplayCB");
                    swimmingpool = jsonObject2.optString("swimmingpoolCB");
                    multipurposehall = jsonObject2.optString("multipurposehallCB");
                    restaurent = jsonObject2.optString("restaurentCB");
                    daycare = jsonObject2.optString("daycareCB");
                    sports = jsonObject2.optString("sportsCB");
                    landscape = jsonObject2.optString("landscapeCB");
                    videosecurity = jsonObject2.optString("videosecurityCB");
                    clubhouse = jsonObject2.optString("clubhouseCB");
                }

                if (!Agent.isEmpty())
                    agentET.setText(Agent);
                if (!vidsource.isEmpty())
                    vidsourceET.setText(vidsource);
                if (!Videoedit.isEmpty())
                    videoeditTV.setText(Videoedit);

                if (!gym.isEmpty())
                    gymCB.setChecked(true);
                if (!kidsplay.isEmpty())
                    kidsplayCB.setChecked(true);
                if (!swimmingpool.isEmpty())
                    swimmingpoolCB.setChecked(true);
                if (!multipurposehall.isEmpty())
                    multipurposehallCB.setChecked(true);
                if (!restaurent.isEmpty())
                    restaurentCB.setChecked(true);
                if (!daycare.isEmpty())
                    daycareCB.setChecked(true);
                if (!sports.isEmpty())
                    sportsCB.setChecked(true);
                if (!landscape.isEmpty())
                    landscapeCB.setChecked(true);
                if (!videosecurity.isEmpty())
                    videosecurityCB.setChecked(true);
                if (!clubhouse.isEmpty())
                    clubhouseCB.setChecked(true);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevIV:

                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.amenityIV:
                if (amenitiesCV.getVisibility() == View.VISIBLE) {

                    amenitiesCV.setVisibility(View.GONE);
                } else {
                    amenitiesCV.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.floorplanIV:
                if (floorCV.getVisibility() == View.VISIBLE) {

                    floorCV.setVisibility(View.GONE);
                } else {
                    floorCV.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.agentIV:
                if (agentCV.getVisibility() == View.VISIBLE) {

                    agentCV.setVisibility(View.GONE);
                } else {
                    agentCV.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.videoIV:
                if (videoCV.getVisibility() == View.VISIBLE) {

                    videoCV.setVisibility(View.GONE);
                } else {
                    videoCV.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.photogalleryIV:
                if (galleryCV.getVisibility() == View.VISIBLE) {

                    galleryCV.setVisibility(View.GONE);
                } else {
                    galleryCV.setVisibility(View.VISIBLE);
                }

                break;

            case R.id.submitBT:
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String jason_data = getDataInJson();
                        if (exists_id > 99) {
                            smartMoveDatabaseAdapter.updateData(AppConstant.KEY_AMENITIES, jason_data, exists_id);
                        } else {
                            smartMoveDatabaseAdapter.updateData(AppConstant.KEY_AMENITIES, jason_data, AppConstant.UNIQUE_ID);
                        }
                        AppConstant.UNIQUE_ID++;
                        commonUtils.set_memory_Storage(false);
                        commonUtils.setUniqueId(AppConstant.UNIQUE_ID);
                        count = 1;
                        Intent intent = new Intent(getActivity(), PushToLiveActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });


                break;
            case R.id.floorplanBT:
                Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
                startActivityForResult(i, 200);

                break;
            case R.id.photogalleryBT:
                String[] items = {"Take Photo ", "Open Gallery", "Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            j = 1;
                            captureImage();
                        }


                        if (item == 1) {
                            j = 2;
                            Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
                            startActivityForResult(i, 300);
                        } else if (item == 2) {
                            alert.dismiss();
                        }
                    }
                });
                alert = builder.create();
                alert.show();
                break;
            case R.id.agentET:
                commonUtils.alertForDropdown(items1, agentET);
                break;
            case R.id.videoeditTV:
                commonUtils.alertForDropdown(items1, videoeditTV);
                break;

        }
    }

    private void captureImage() {
        camera_picture = new ArrayList<CustomGallery>();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        picturePath = String.valueOf(new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg"));

        destination = new File(picturePath);

        Log.e("FileDestination", "" + destination);
        Log.e("picturePath", "" + picturePath);
        CustomGallery item = new CustomGallery();
        item.sdcardPath = picturePath;
        gallery_json_object.put(picturePath);
        camera_picture.add(item);

       // adapter1.addAll(dataT);
      //adapter1.notifyDataSetChanged();
      //  galleryGV.setAdapter(adapter1);

        file = new File(picturePath);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE)  {
            // adapter.clear();

            BitmapFactory.Options options;

            options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            Bitmap bitmap = BitmapFactory.decodeFile(
                    destination.getAbsolutePath(), options);


            adapter1.addAll(camera_picture);
            galleryGV.setAdapter(adapter1);

           /* if (bitmap != null) {
                imagesArray.add(bitmap);
            }*/
     /*       if (exists_id < 99) {

                ImageSetterAdapter imageSetterAdapter = new ImageSetterAdapter(getActivity(), imagesArray);
                galleryGV.setAdapter(imageSetterAdapter);

            } else {
                imageSetterAdapter.notifyDataSetChanged();
            }
            galleryGV.setAdapter(imageSetterAdapter);
*/
        }
        else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            String[] all_path = data.getStringArrayExtra("all_path");

            ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();

            for (String string : all_path) {
                CustomGallery item = new CustomGallery();
                item.sdcardPath = string;
                floor_json_object.put(string);
                dataT.add(item);
            }
            Log.d("floor_json_object", "" + floor_json_object);
            // viewSwitcher.setDisplayedChild(0);
            //adapter = new GalleryAdapter(getActivity(), imageLoader);
            adapter.addAll(dataT);
            floorGV.setAdapter(adapter);
        } else if (requestCode == 300 && resultCode == Activity.RESULT_OK) {
            String[] all_path = data.getStringArrayExtra("all_path");

            ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();

            for (String string : all_path) {
                CustomGallery item = new CustomGallery();
                item.sdcardPath = string;
                gallery_json_object.put(string);
                dataT.add(item);


            }
            // viewSwitcher.setDisplayedChild(0);
            adapter1.addAll(dataT);
            galleryGV.setAdapter(adapter1);
        }
    }
    @SuppressLint("InlinedApi")
    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission
                    .WRITE_EXTERNAL_STORAGE}, 1234);
        }
    }
    }

/*
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE || requestCode == RESULT_LOAD_IMAGE2 && resultCode == RESULT_OK
                && null != data) {

            selectedImage = data.getData();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                // Process the uri...

                picturePath = cursor.getString(columnIndex);
                if (!picturePath.isEmpty() && requestCode == RESULT_LOAD_IMAGE2) {
                    if (exists_id < 99) {
                        gallery_json_object.put(picturePath);
                    } else {
                        jsonArray1.put(picturePath);
                    }

                }
                if (!picturePath.isEmpty() && requestCode == RESULT_LOAD_IMAGE)

                {
                    if (exists_id < 99) {
                        floor_json_object.put(picturePath);
                    } else {
                        jsonArray2.put(picturePath);
                    }
                }
                Log.d("picturrr", "" + picturePath);

                cursor.close();
         *//*
*/
/*   }*//*
*/
/*



                try {

                    BitmapFactory.Options options;

                    options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    bitmap = commonUtils.ShrinkBitmap(picturePath, 100, 100);
                    // bitmap = BitmapFactory.decodeFile(picturePath, options);
                    if (requestCode == RESULT_LOAD_IMAGE) {
                        floor_images_array_list.add(bitmap);

                        if (exists_id < 99) {

                            FloorImageSetterAdapter floorImageSetterAdapter = new FloorImageSetterAdapter(getActivity(), floor_images_array_list);
                            floorGV.setAdapter(floorImageSetterAdapter);

                        } else {
                            floorImageSetterAdapter.notifyDataSetChanged();
                        }
                    } else {

                        imagesArray.add(bitmap);
                    }

                    if (exists_id < 99) {

                        ImageSetterAdapter imageSetterAdapter = new ImageSetterAdapter(getActivity(), imagesArray);
                        galleryGV.setAdapter(imageSetterAdapter);

                    } else {
                        imageSetterAdapter.notifyDataSetChanged();
                    }
                    Log.e("bitmapmedia", "" + bitmap);
                    file = new File(picturePath);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


            }

}


*/
