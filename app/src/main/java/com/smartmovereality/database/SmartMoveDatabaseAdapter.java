package com.smartmovereality.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.smartmovereality.AppConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc20 on 4/8/16.
 */

    public class SmartMoveDatabaseAdapter extends SQLiteOpenHelper {

        private static final String DATABASE_NAME = "smartmovedatabase";
        private static final String TABLE_NAME = "SMARTMOVETABLE";
        private static final int DATABASE_VERSION = 1;
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + AppConstant.KEY_UNIQUE_ID + " INTEGER PRIMARY KEY," + AppConstant.KEY_SELECT_PROJECT + " VARCHAR(255),"
                + AppConstant.KEY_SELECT_CATEGORY + " VARCHAR(255),"+ AppConstant.KEY_PROPERTY_AREA + " VARCHAR(255),"+ AppConstant.KEY_CHECK_AVAILABILITY
                + " VARCHAR(255),"+AppConstant.KEY_ADDITIONAL_INFORMATION + " VARCHAR(255),"+ AppConstant.KEY_AMENITIES + " VARCHAR(255));";


        public SmartMoveDatabaseAdapter(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            Log.d("OnCreatecalled", "" + db);
            db.execSQL(CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d("upgradecalled", "");
            db.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME);
            onCreate(db);
        }
        public long insertData(int unique_id,String object1,String object2,String object3,String object4,String object5,String object6){
            SQLiteDatabase db=getWritableDatabase();
            ContentValues contentValues=new ContentValues();
            contentValues.put(AppConstant.KEY_UNIQUE_ID, unique_id);
            contentValues.put(AppConstant.KEY_SELECT_PROJECT, object1);
            long id=db.insert(SmartMoveDatabaseAdapter.TABLE_NAME, null, contentValues);
            db.close();
            return id;
        }
 public void updateData(String ColumnName, String newValue,int id){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ColumnName,newValue);
        db.update(TABLE_NAME, cv, "unique_id=" + id, null);

    }


        public String getSelectItems(int id,String key){

            String selected_frame="";

            SQLiteDatabase db=getWritableDatabase();
            Cursor cursor = db.query(TABLE_NAME, new String[]{key}, AppConstant.KEY_UNIQUE_ID + "=?", new String[]
                    {String.valueOf(id)}, null, null, null, null);

            if(cursor.moveToFirst()){
                do{



                   selected_frame= cursor.getString(cursor.getColumnIndex(key));


                }while(cursor.moveToNext());
            }

            db.close();
            return selected_frame;
        }


    public ArrayList<HashMap<String,String>> getData(){
        SQLiteDatabase db=getWritableDatabase();
        ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(selectQuery, null);
        while (cursor.moveToNext()) {
            String id = cursor.getString(cursor.getColumnIndex(AppConstant.KEY_UNIQUE_ID));
            String response1 = cursor.getString(cursor.getColumnIndex(AppConstant.KEY_PROPERTY_AREA));
            String response2 = cursor.getString(cursor.getColumnIndex(AppConstant.KEY_CHECK_AVAILABILITY));
            String response3 = cursor.getString(cursor.getColumnIndex(AppConstant.KEY_ADDITIONAL_INFORMATION));
            String response4 = cursor.getString(cursor.getColumnIndex(AppConstant.KEY_AMENITIES));
            HashMap<String,String> hashMap=new HashMap();
            hashMap.put("id", id);
            hashMap.put("response1", response1);
            hashMap.put("response2", response2);
            hashMap.put("response3", response3);
            hashMap.put("response4", response4);
            arrayList.add(hashMap);

        }
        db.close();

        return arrayList;
    }

    }

