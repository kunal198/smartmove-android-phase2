package com.smartmovereality;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 4/5/16.
 */
public class AppConstant {

    public static String KEY_UNIQUE_ID="unique_id";
    public static int UNIQUE_ID=100;
    public static String ADD_PROPERTY_URL= "http://smartmoverealty.in/?post_prop_API=true";
    public static String URL="http://smartmoverealty.in/?";
    public static String LOGIN_URL= "http://smartmoverealty.in/?u_name=username&u_pass=password";

    public static String ALERT_MESSAGE="User name and Password cannot be blank";
    public static String INCORRECT_CARDENTIALS="Incorrect user name or Password";
    public static String USER_NAME=" User name cannot be blank";
    public static String PASSWORD="Password cannot be blank";
    public static String NETWORK_ERROR="Please connect your device to internet";
    public static final String KEY_SELECT_PROJECT="SelectProject";
    public static final String KEY_PROPERTY_AREA="property_area";
    public static final String KEY_SELECT_CATEGORY="SelectCategory";
    public static final String KEY_CHECK_AVAILABILITY="checkavailability";
    public static final String KEY_ADDITIONAL_INFORMATION="additionalinformation";
    public static final String KEY_AMENITIES="amenities";
     public static Boolean flag=false;

    public static String title;
}
