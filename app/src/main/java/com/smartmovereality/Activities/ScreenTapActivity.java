package com.smartmovereality.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartmovereality.AppConstant;
import com.smartmovereality.CommonUtils;
import com.smartmovereality.R;

/**
 * Created by brst-pc20 on 4/4/16.
 */
public class ScreenTapActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView addIV;
    Toolbar toolbar;
    TextView taphereTV,addpropertyTV;
    CommonUtils commonUtils;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_tap);

        init();


    }

    public void init() {
        commonUtils = new CommonUtils(this);
        toolbar = (Toolbar) findViewById(R.id.toolbarIC);
        toolbar.setTitle(" ");

        setSupportActionBar(toolbar);
        addIV = (ImageView) toolbar.findViewById(R.id.addIV);
        addIV.setVisibility(View.VISIBLE);
        taphereTV = (TextView) findViewById(R.id.taphereTV);
        taphereTV.setOnClickListener(this);
        addIV.setOnClickListener(this);
        if (commonUtils.getUniqueId()>1) {
            int id = commonUtils.getUniqueId();
            AppConstant.UNIQUE_ID = id;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.addIV:

                commonUtils.setUniqueId(AppConstant.UNIQUE_ID);
                Intent intent = new Intent(ScreenTapActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

                break;
            case R.id.taphereTV:
                int id1 = commonUtils.getUniqueId();
                if (id1 < 0 ) {
                    commonUtils.setUniqueId(AppConstant.UNIQUE_ID);

                }
                Intent intent1 = new Intent(ScreenTapActivity.this, MainActivity.class);
                startActivity(intent1);
                finish();
                break;
        }
    }
}
