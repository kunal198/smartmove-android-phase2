package com.smartmovereality.fragments;

import android.support.v4.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartmovereality.Activities.MainActivity;
import com.smartmovereality.AppConstant;
import com.smartmovereality.CommonUtils;
import com.smartmovereality.R;
import com.smartmovereality.database.SmartMoveDatabaseAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 4/2/16.
 */
public class AdditionalInformationFragment extends Fragment implements View.OnClickListener {
    ImageView nextIV,additionalIV,detailIV,prevIV;
    TextView areatypeTV;
    EditText priceET,areaET,areatypeET,bedroomsET,bathroomsET,ownerET,contactET,emailET,propertyET,unitsET
            ,approvalsET,urnameET,propertynumberET,societyET,floorsET,towersET;
    String items_area[]={"area1","area2","area3"};
    CommonUtils commonUtils;
    SmartMoveDatabaseAdapter smartMoveDatabaseAdapter;
    int exists_id;
    CardView additionalinfoCV,detailCV;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.additional_information,container,false);
        init(view);
        return view;
    }
    public void init(View view) {
        prevIV = (ImageView) view.findViewById(R.id.prevIV);
        prevIV.setOnClickListener(this);
        nextIV = (ImageView) view.findViewById(R.id.nextIV);
        priceET = (EditText) view.findViewById(R.id.priceET);
        areaET = (EditText) view.findViewById(R.id.areaET);
        bedroomsET = (EditText) view.findViewById(R.id.bedroomsET);
        bathroomsET = (EditText) view.findViewById(R.id.bathroomsET);
        ownerET = (EditText) view.findViewById(R.id.ownerET);
        contactET = (EditText) view.findViewById(R.id.contactET);
        emailET = (EditText) view.findViewById(R.id.emailET);
        propertyET = (EditText) view.findViewById(R.id.propertyET);
        unitsET = (EditText) view.findViewById(R.id.unitsET);
        approvalsET = (EditText) view.findViewById(R.id.approvalsET);
        urnameET = (EditText) view.findViewById(R.id.urnameET);
        propertynumberET = (EditText) view.findViewById(R.id.propertynumberET);
        societyET = (EditText) view.findViewById(R.id.societyET);
        floorsET = (EditText) view.findViewById(R.id.floorsET);
        towersET = (EditText) view.findViewById(R.id.towersET);
        areatypeTV= (TextView) view.findViewById(R.id.areatypeTV);
        detailCV= (CardView) view.findViewById(R.id.detailCV);
        additionalinfoCV= (CardView) view.findViewById(R.id.additionalinfoCV);
        additionalIV= (ImageView) view.findViewById(R.id.additionalIV);
        detailIV= (ImageView) view.findViewById(R.id.detailIV);
        commonUtils=new CommonUtils(getActivity());
        smartMoveDatabaseAdapter=new SmartMoveDatabaseAdapter(getActivity());
        nextIV.setOnClickListener(this);

        areatypeTV.setOnClickListener(this);
        detailIV.setOnClickListener(this);
        additionalIV.setOnClickListener(this);
        editData();
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.prevIV:

                getFragmentManager().popBackStackImmediate();
                break;


            case R.id.backIV:

                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.nextIV:
                String data=getDataToJson();
                if (exists_id>99) {
                    smartMoveDatabaseAdapter.updateData(AppConstant.KEY_ADDITIONAL_INFORMATION,data,exists_id);
                }
                else {smartMoveDatabaseAdapter.updateData(AppConstant.KEY_ADDITIONAL_INFORMATION,data,AppConstant.UNIQUE_ID);
                }

                if (exists_id>99){
                    Bundle bundle=new Bundle();
                    bundle.putInt("fwd_id", exists_id);
                    AmenitiesFragment amenitiesFragment=new AmenitiesFragment();
                    amenitiesFragment.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, amenitiesFragment).addToBackStack(null);
                    fragmentTransaction.commit();
                }
                else {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, new AmenitiesFragment()).addToBackStack(null);
                    fragmentTransaction.commit();
                }

                break;
            case  R.id.areatypeTV:
                 commonUtils.alertForDropdown(items_area,areatypeTV);
                         break;
            case R.id.additionalIV:
                if (additionalinfoCV.getVisibility()==View.VISIBLE){

                    additionalinfoCV.setVisibility(View.INVISIBLE);
                }
                else {
                    additionalinfoCV.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.detailIV:
                if (detailCV.getVisibility()==View.VISIBLE){

                    detailCV.setVisibility(View.GONE);
                }
                else {
                    detailCV.setVisibility(View.VISIBLE);
                }
                break;
        }
    }
    public void editData(){
        String str_editable_data;
        Bundle bundle=getArguments();
        if (bundle!=null)
        {
            exists_id=   bundle.getInt("fwd_id");
        }
        if (exists_id!=0)
        {
            str_editable_data=  smartMoveDatabaseAdapter.getSelectItems(exists_id, AppConstant.KEY_ADDITIONAL_INFORMATION);
            Log.d("str_editable_data",""+str_editable_data);
            try {
                JSONObject jsonObject = new JSONObject(str_editable_data);
                JSONArray jsonArray = jsonObject.optJSONArray("jsonarray");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                    String society=jsonObject1.optString("society");
                    String approval=jsonObject1.optString("approval");
                    String property=jsonObject1.optString("property");
                    String bathroom=jsonObject1.optString("bathroom");
                    String contact=jsonObject1.optString("contact");
                    String units=jsonObject1.optString("units");
                    String bedroom=jsonObject1.optString("bedroom");
                    String area_type=jsonObject1.optString("area_type");
                    String property_number=jsonObject1.optString("property_number");
                    String tower=jsonObject1.optString("tower");
                    String area=jsonObject1.optString("area");
                    String price=jsonObject1.optString("price");
                    String floors=jsonObject1.optString("floors");
                    String email=jsonObject1.optString("email");
                    String owner=jsonObject1.optString("owner");
                    String urname=jsonObject1.optString("urname");
                    if (!society.isEmpty())
                        societyET.setText(society);
                    if (!approval.isEmpty())
                        approvalsET.setText(approval);
                    if (!property.isEmpty())
                        propertyET.setText(property);
                    if (!bathroom.isEmpty())
                        bathroomsET.setText(bathroom);
                    if (!contact.isEmpty())
                        contactET.setText(contact);
                    if (!units.isEmpty())
                        unitsET.setText(units);
                    if (!bedroom.isEmpty())
                        bedroomsET.setText(bedroom);
                    if (!area_type.isEmpty())
                        areatypeTV.setText(area_type);
                    if (!property_number.isEmpty())
                        propertynumberET.setText(property_number);
                    if (!tower.isEmpty())
                        towersET.setText(tower);
                    if (!area.isEmpty())
                        areaET.setText(area);
                    if (!price.isEmpty())
                        priceET.setText(price);
                    if (!floors.isEmpty())
                        floorsET.setText(floors);
                    if (!email.isEmpty())
                        emailET.setText(email);
                    if (!owner.isEmpty())
                        ownerET.setText(owner);
                    if (!urname.isEmpty())
                        urnameET.setText(urname);
                }
            }catch(JSONException e){
                e.printStackTrace();
            }
        }
    }
    public String getDataToJson() {
        ArrayList<JSONObject> arrayList = new ArrayList<>();
        JSONArray jsonArray=new JSONArray();
        String price = priceET.getText().toString();
        String area = areaET.getText().toString();
        String area_type = areatypeTV.getText().toString();
        String bedroom = bedroomsET.getText().toString();
        String bathroom = bathroomsET.getText().toString();
        String owner = ownerET.getText().toString();
        String contact = contactET.getText().toString();
        String email = emailET.getText().toString();
        String property = propertyET.getText().toString();
        String units = unitsET.getText().toString();
        String approval = approvalsET.getText().toString();
        String urname = urnameET.getText().toString();
        String property_number = propertynumberET.getText().toString();
        String society = societyET.getText().toString();
        String floors = floorsET.getText().toString();
        String tower = towersET.getText().toString();
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();
        try {
            if (!price.isEmpty()) {
                jsonObject1.put("price", price);

            }
            if (!area.isEmpty()) {
                jsonObject1.put("area", area);

            }
            if (!area_type.isEmpty()) {
                jsonObject1.put("area_type", area_type);

            }
            if (!bedroom.isEmpty()) {
                jsonObject1.put("bedroom", bedroom);

            }
            if (!bathroom.isEmpty()) {
                jsonObject1.put("bathroom", bathroom);

            }
            if (!owner.isEmpty()) {
                jsonObject1.put("owner", owner);

            }
            if (!contact.isEmpty()) {
                jsonObject1.put("contact", contact);

            }if (!email.isEmpty()) {
                jsonObject1.put("email", email);

            }
            if (!property.isEmpty()) {
                jsonObject1.put("property", property);

            }

            if (!units.isEmpty()) {
                jsonObject1.put("units", units);

            }
            if (!approval.isEmpty()) {
                jsonObject1.put("approval", approval);

            }
            if (!urname.isEmpty()) {
                jsonObject1.put("urname", urname);

            }
            if (!property_number.isEmpty()) {
                jsonObject1.put("property_number", property_number);

            }
            if (!society.isEmpty()) {
                jsonObject1.put("society", society);

            }
            if (!floors.isEmpty()) {
                jsonObject1.put("floors", floors);

            }
            if (!tower.isEmpty()) {
                jsonObject1.put("tower", tower);

            }
            jsonArray.put(jsonObject1);

            if (!jsonArray.toString().isEmpty())
                jsonObject.put("jsonarray",jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("obj", "" + jsonObject);
        String object5=jsonObject.toString();
        Log.d("objobject4", "" + object5);
        return object5;

    }

}
