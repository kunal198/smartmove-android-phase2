package com.smartmovereality.Activities;

/**
 * Created by brst-pc20 on 4/25/16.
 */
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import android.app.Application;

import com.smartmovereality.R;

public class Acra {


    @ReportsCrashes(formKey = "", // will not be used
            mailTo = "jatin.kumar@brihaspatitech.com", mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast_text)
    public class QuickMilkApplication extends Application {
        @Override
        public void onCreate() {
            ACRA.init(this);
            super.onCreate();
        }
    }
}
