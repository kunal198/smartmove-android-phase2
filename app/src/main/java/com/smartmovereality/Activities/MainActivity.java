package com.smartmovereality.Activities;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.smartmovereality.AppConstant;
import com.smartmovereality.R;
import com.smartmovereality.database.SmartMoveDatabaseAdapter;
import com.smartmovereality.fragments.SelectProjectFragment;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by brst-pc20 on 4/2/16.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
int exist_id;
    Toolbar toolbar;TextView addpropertyTV;
    public static ImageView backIV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        init();
    }
    public void init()
    {
    SmartMoveDatabaseAdapter smartMoveDatabaseAdapter=new SmartMoveDatabaseAdapter(this);
        Intent intent=getIntent();
        exist_id = intent.getIntExtra("selected_id", 0);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        addpropertyTV= (TextView) findViewById(R.id.addpropertyTV);

        if (exist_id>99) {

            String str_editable_data = smartMoveDatabaseAdapter.getSelectItems(exist_id, AppConstant.KEY_PROPERTY_AREA);
            Log.d("str_editable_data", "" + str_editable_data);
            try {
                JSONObject jsonObject = new JSONObject(str_editable_data);
                JSONArray jsonArray = jsonObject.optJSONArray("jsonarray");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                    String zip = jsonObject1.optString("zip");
                    String title = jsonObject1.optString("title");
                    if (!title.isEmpty())
                        addpropertyTV.setText(title);
                    else
                        addpropertyTV.setText("EDIT PROPERTY");

                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, new SelectProjectFragment(), "MY_FRAGMENT");
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment_byTag = fm.findFragmentByTag("MY_FRAGMENT");
        if (fragment_byTag != null && fragment_byTag.isVisible()) {
         Intent intent=new Intent(MainActivity.this,PushToLiveActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void onClick(View v) {

    }


}
