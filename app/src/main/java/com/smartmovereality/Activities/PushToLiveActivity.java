package com.smartmovereality.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.sax.Element;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.smartmovereality.CommonUtils;
import com.smartmovereality.Data;
import com.smartmovereality.R;
import com.smartmovereality.database.SmartMoveDatabaseAdapter;
import com.smartmovereality.fragments.SelectProjectFragment;
import android.app.ProgressDialog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import Adapters.PropertyHouseAdapter;

/**
 * Created by brst-pc20 on 4/2/16.
 */
public class PushToLiveActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    ListView houseLV;
    Toolbar toolbar;
    ImageView addIV;
    Button savelocallyBT, pushliveBT;

    SmartMoveDatabaseAdapter smartMoveDatabaseAdapter;
    ArrayList<HashMap<String, String>> arrayList;
    ArrayList<String> select_project_array_list;
    ArrayList<String> select_category_array_list;
    ArrayList<String> check_availability_array_list;
    ArrayList<String> additional_information_array_list;
    ArrayList<String> amenities_array_list;
    ArrayList<Data> data_list = new ArrayList();
    ArrayList<Data> data_list1 = new ArrayList();
    ArrayList<Data> data_list2 = new ArrayList();
    ArrayList<Data> data_list3 = new ArrayList();
    ArrayList<String> gallery_image;
    CommonUtils commonUtils;
    ProgressDialog dlg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.push_to_live);
        init();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
             new Display().execute();

            }
        });

    }

    public void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        addIV = (ImageView) toolbar.findViewById(R.id.addIV);
        savelocallyBT = (Button) findViewById(R.id.savelocallyBT);
        pushliveBT = (Button) findViewById(R.id.pushliveBT);
        addIV.setOnClickListener(this);
        smartMoveDatabaseAdapter = new SmartMoveDatabaseAdapter(this);
        houseLV = (ListView) findViewById(R.id.houseLV);
        commonUtils = new CommonUtils(this);
        dlg=new ProgressDialog(this);
    }

    private  class Display extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... params) {

                arrayList = new ArrayList<>();
                gallery_image = new ArrayList<>();
                select_project_array_list = new ArrayList<>();
                select_category_array_list = new ArrayList<>();
                check_availability_array_list = new ArrayList<>();
                additional_information_array_list = new ArrayList<>();
                amenities_array_list = new ArrayList<>();
                arrayList = smartMoveDatabaseAdapter.getData();


                for (int i = 0; i < arrayList.size(); i++) {
                    select_project_array_list.add(arrayList.get(i).get("id"));
                    select_category_array_list.add(arrayList.get(i).get("response1"));
                    check_availability_array_list.add(arrayList.get(i).get("response2"));
                    additional_information_array_list.add(arrayList.get(i).get("response3"));
                    amenities_array_list.add(arrayList.get(i).get("response4"));
                }

                Log.d("amenities_array_list", "" + amenities_array_list);

                Log.d("select_project", "" + select_project_array_list);

                Log.d("select_category", "" + select_category_array_list);

                Log.d("check_availability", "" + check_availability_array_list);

                Log.d("additional_information", "" + additional_information_array_list);




                if (!select_category_array_list.isEmpty()) {
                    for (int j = 0; j < select_category_array_list.size(); j++) {
                        Log.d("str_data", "" + select_category_array_list.get(j));
                        String str_data = select_category_array_list.get(j);
                        if (str_data != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(str_data);
                                Log.d("str_data", "" + jsonObject);
                                JSONArray jsonArray = jsonObject.getJSONArray("jsonarray");
                                Log.d("jsonarray1", "" + jsonArray);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Data data = new Data();
                                    JSONObject obj = jsonArray.optJSONObject(i);
                                    Log.d("logo", "" + obj.optString(""));
                                    data.setTitle(obj.optString("title"));
                                    data.setCity(obj.optString("city"));
                                    data.setNeighbour(obj.optString("address"));
                                    data_list.add(data);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                if (!check_availability_array_list.isEmpty()) {
                    for (int j = 0; j < check_availability_array_list.size(); j++) {
                        String str_data = check_availability_array_list.get(j);
                        if (str_data != null) {
                            try {

                                Data data = new Data();
                                JSONObject jsonObject = new JSONObject(str_data);
                                JSONArray jsonArray = jsonObject.getJSONArray("jsonobject");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.optJSONObject(i);
                                    data.setStatus(obj.optString("availability"));
                                    data_list1.add(data);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                if (!additional_information_array_list.isEmpty()) {
                    for (int j = 0; j < additional_information_array_list.size(); j++) {
                        String str_data = additional_information_array_list.get(j);
                        if (str_data != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(str_data);
                                JSONArray jsonArray = jsonObject.getJSONArray("jsonarray");
                                Log.d("jsonarray4", "" + jsonArray);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Data data = new Data();
                                    JSONObject obj = jsonArray.optJSONObject(i);
                                    Log.d("logo", "" + obj.optString(""));
                                    data.setArea(obj.optString("area"));
                                    data.setPrice(obj.optString("price"));
                                    data_list2.add(data);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                if (!amenities_array_list.isEmpty()) {
                    for (int j = 0; j < amenities_array_list.size(); j++) {
                        Log.d("ameniti", "" + amenities_array_list.size());
                        String str_data = amenities_array_list.get(j);
                        if (str_data != null) {
                            try {
                                Data data = new Data();
                                JSONObject jsonObject = new JSONObject(str_data);
                                JSONObject jsonObject1 = jsonObject.optJSONObject("video_source_data");
                                JSONArray jsonArray = jsonObject1.optJSONArray("gallery_json_object");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String picturepath = jsonArray.optString(0);
                                    Bitmap myBitmap = commonUtils.ShrinkBitmap(picturepath, 500, 400);

                                    data.setGallery_image(myBitmap);


                                }
                                data_list3.add(data);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                Log.d("listtttttttttttt", "" + data_list);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (dlg!=null)
            dlg.dismiss();
            if (data_list.isEmpty() && data_list1.isEmpty() && data_list2.isEmpty() && data_list3.isEmpty()) {
                Intent i = new Intent(PushToLiveActivity.this, ScreenTapActivity.class);
                startActivity(i);
            }
            Collections.reverse(data_list);
            Collections.reverse(data_list1);
            Collections.reverse(data_list2);
            Collections.reverse(data_list3);
            Collections.reverse(select_project_array_list);

            PropertyHouseAdapter propertyHouseAdapter = new PropertyHouseAdapter(PushToLiveActivity.this, data_list, data_list1, data_list2, data_list3);
            houseLV.setAdapter(propertyHouseAdapter);
            enableListner();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dlg.setMessage("Loading data....");
            dlg.setCancelable(false);
            dlg.show();
        }
    }


    private void enableListner() {
        houseLV.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int selected_id = Integer.parseInt(select_project_array_list.get(position));

        Intent intent = new Intent(PushToLiveActivity.this, MainActivity.class);
        intent.putExtra("selected_id", selected_id);
        startActivity(intent);
        finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.addIV:

                Intent intent = new Intent(PushToLiveActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

                break;
        }
    }


}