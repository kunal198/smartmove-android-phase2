package Adapters;

import android.content.Context;
import android.graphics.Bitmap;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.smartmovereality.Data;
import com.smartmovereality.R;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 4/12/16.
 */
public class PropertyHouseAdapter extends ArrayAdapter<Data>{
    Context context;

    ArrayList<Data> data_list = new ArrayList<Data>();
    ArrayList<Data> data_list1 = new ArrayList<Data>();
    ArrayList<Data> data_list2 = new ArrayList<Data>();
    ArrayList<Data> data_list3 = new ArrayList<Data>();
    public PropertyHouseAdapter(Context context, ArrayList<Data> data_list,ArrayList<Data> data_list1,ArrayList<Data> data_list2,ArrayList<Data> data_list3) {
        super(context, R.layout.property_custom_list, R.id.titleTV, data_list);
        this.context = context;
        this.data_list = data_list;
        this.data_list1=data_list1;
        this.data_list2=data_list2;
       this.data_list3=data_list3;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        viewHolder mhHolder = null;

        if (view == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.property_custom_list, parent, false);
            mhHolder = new viewHolder(view);
            view.setTag(mhHolder);
        } else {
            mhHolder = (viewHolder) view.getTag();
        }
        Data data1 = data_list.get(position);

        Log.d("log111111", "mkhkjnjk" + data1.getTitle());
        if(data_list.size()>0) {
            mhHolder.titleTV.setText(data_list.get(position).getTitle());
            mhHolder.cityTV.setText(data_list.get(position).getCity());
            mhHolder.localityTV.setText(data_list.get(position).getNeighbour());
        }
        if(data_list1.size()>0) {
            mhHolder.statusTV.setText(data_list1.get(position).getStatus());
        }
        if (data_list2.size()>0) {
            if (data_list2.get(position).getPrice().isEmpty())
                mhHolder.priceTV.setVisibility(View.GONE);

            mhHolder.priceTV.setText(data_list2.get(position).getPrice());
            mhHolder.areaTV.setText(data_list2.get(position).getArea());
           // mhHolder.highlightsTV.setText(data_list2.get(position).getHighlights());
        }
        if (data_list3.size()>0) {
            Log.d("datasize", "" + data_list3);

                Bitmap bitmap = data_list3.get(position).getGallery_image();
            if(bitmap!=null)
                mhHolder.rowimageIV.setImageBitmap(bitmap);
            else {
                // mhHolder.galleryIVV.setImageDrawable(context.getResources().getDrawable(R.mipmap.no_imagee));


                mhHolder.rowimageIV.setImageResource(R.drawable.no_image_icon);
            }
        }

        return  view;
    }




    private class viewHolder {

        TextView titleTV, cityTV, localityTV, statusTV, areaTV, highlightsTV, priceTV;
        ImageView rowimageIV;
        viewHolder(View view) {

            titleTV = (TextView) view.findViewById(R.id.titleTV);
            cityTV = (TextView) view.findViewById(R.id.cityTV);
            localityTV = (TextView) view.findViewById(R.id.localityTV);
            statusTV = (TextView) view.findViewById(R.id.statusTV);
            areaTV = (TextView) view.findViewById(R.id.areaTV);
            highlightsTV = (TextView) view.findViewById(R.id.highlightsTV);
            priceTV = (TextView) view.findViewById(R.id.priceTV);
            rowimageIV= (ImageView) view.findViewById(R.id.rowimageIV);

        }

    }
}
