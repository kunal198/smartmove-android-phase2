package com.smartmovereality.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.smartmovereality.CommonUtils;
import com.smartmovereality.R;

/**
 * Created by brst-pc20 on 4/5/16.
 */
public class SplashActivity extends Activity {
CommonUtils commonUtils;
    private static int SPLASH_TIME_OUT = 1500;
    int id;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        commonUtils=new CommonUtils(this);
          new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (id==-1){

                    Intent i = new Intent(SplashActivity.this, ScreenTapActivity.class);
                    startActivity(i);
                }

                if(commonUtils.getUserLogin() != -1){

                    Intent i = new Intent(SplashActivity.this, PushToLiveActivity.class);
                    startActivity(i);

                }
                else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);

            }

                finish();
            }
        }, SPLASH_TIME_OUT);
    }



}

