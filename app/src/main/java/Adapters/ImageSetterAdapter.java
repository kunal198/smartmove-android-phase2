package Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartmovereality.Data;
import com.smartmovereality.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 4/18/16.
 */
public class ImageSetterAdapter extends ArrayAdapter<String> {
    Context context;
    ArrayList<Bitmap> store_images_gallery;

    public ImageSetterAdapter(Context context, ArrayList store_images_gallery) {
        super(context, R.layout.single_row, R.id.galleryIVV,store_images_gallery);
        this.context = context;
        this.store_images_gallery = store_images_gallery;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        viewHolder mhHolder = null;
        if (view == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.single_row, parent, false);
            mhHolder = new viewHolder(view);
            view.setTag(mhHolder);
        } else {
            mhHolder = (viewHolder) view.getTag();
        }


         Bitmap bitmap=store_images_gallery.get(position);
        if (bitmap!=null)
        mhHolder.galleryIVV.setImageBitmap(bitmap);

        return view;
    }


    private class viewHolder {
        ImageView galleryIVV;

        viewHolder(View view) {

            galleryIVV = (ImageView) view.findViewById(R.id.galleryIVV);

        }

    }
}

