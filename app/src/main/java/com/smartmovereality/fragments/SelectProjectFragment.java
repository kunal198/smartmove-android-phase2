package com.smartmovereality.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.smartmovereality.Activities.MainActivity;
import com.smartmovereality.Activities.PushToLiveActivity;
import com.smartmovereality.AppConstant;
import com.smartmovereality.CommonUtils;
import com.smartmovereality.R;
import com.smartmovereality.database.SmartMoveDatabaseAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc20 on 4/2/16.
 */
public class SelectProjectFragment extends Fragment implements View.OnClickListener,RadioGroup.OnCheckedChangeListener{

    ImageView projectIV, propertyareaIV,nextIV,prevIV;
    View view1,view2;
    RadioGroup projectRD,propertyareaRD;
    Boolean flag=false,flag2=false;
    RadioButton projectRB,propertyareaRB,megaprojectRB,resaleprojectRB,rentprojectRB,commercialRB,propertareaRB,flatRB,industryRB,kothiRB,plotRB,otherRB;
    CardView megaprojectCV,propertyareaCV,propertyareaCV1;

    CommonUtils commonUtils;
    String project_rb,property_area_rb;
    SmartMoveDatabaseAdapter smartMoveDatabaseAdapter;
    String object1;
    ArrayList<HashMap<String, String>> edit_screen_list;
    int exist_id;
    Toolbar toolbar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_property, container, false);
        init(view);
        return view;
    }

    public void init(View view) {
        commonUtils=new CommonUtils(getActivity());
        if (commonUtils.getUniqueId()>1) {
            int id = commonUtils.getUniqueId();
            AppConstant.UNIQUE_ID = id;

        }
        smartMoveDatabaseAdapter=new SmartMoveDatabaseAdapter(getActivity());
        nextIV = (ImageView) view.findViewById(R.id.nextIV);
        prevIV= (ImageView) view.findViewById(R.id.prevIV);
        projectIV = (ImageView) view.findViewById(R.id.projectIV);
        propertyareaIV = (ImageView) view.findViewById(R.id.propertyareaIV);
        nextIV= (ImageView) view.findViewById(R.id.nextIV);
        view1=view.findViewById(R.id.view1);
        view2=view.findViewById(R.id.view);
        projectRD= (RadioGroup) view.findViewById(R.id.projectRD);
        propertyareaRD= (RadioGroup) view.findViewById(R.id.propertyareaRD);
        megaprojectRB= (RadioButton) view.findViewById(R.id.megaprojectRB);
        resaleprojectRB= (RadioButton) view.findViewById(R.id.resaleprojectRB);
        rentprojectRB= (RadioButton) view.findViewById(R.id.rentprojectRB);
        commercialRB= (RadioButton) view.findViewById(R.id.commercialRB);
        propertareaRB= (RadioButton) view.findViewById(R.id.propertareaRB);
        projectRD.setOnCheckedChangeListener(this);
        flatRB= (RadioButton) view.findViewById(R.id.flatRB);
        industryRB= (RadioButton) view.findViewById(R.id.industryRB);
        kothiRB= (RadioButton) view.findViewById(R.id.kothiRB);
        plotRB= (RadioButton) view.findViewById(R.id.plotRB);
        otherRB= (RadioButton) view.findViewById(R.id.otherRB);
        megaprojectCV= (CardView) view.findViewById(R.id.megaprojectCV);
        propertyareaCV= (CardView) view.findViewById(R.id.propertyareaCV);
        propertyareaCV1= (CardView) view.findViewById(R.id.propertyareaCV1);
        edit_screen_list=new  ArrayList<HashMap<String, String>>();
        nextIV.setOnClickListener(this);
        projectIV.setOnClickListener(this);
        propertyareaIV.setOnClickListener(this);
        nextIV.setOnClickListener(this);
        prevIV.setOnClickListener(this);
        propertyareaCV.setOnClickListener(this);
        editData();
         }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevIV:
                Intent intent=new Intent(getActivity(),PushToLiveActivity.class);
                getActivity().startActivity(intent);
                break;
            case R.id.nextIV:
                int checkid = projectRD.getCheckedRadioButtonId();
                String idd=""+checkid;
                if(!idd.isEmpty()) {
                    projectRB = (RadioButton) getView().findViewById(checkid);
                }int checkid1 = propertyareaRD.getCheckedRadioButtonId();
                propertyareaRB = (RadioButton) getView().findViewById(checkid1);
                if(projectRB!=null)
                project_rb=projectRB.getText().toString();
                if(propertyareaRB!=null)
                property_area_rb=propertyareaRB.getText().toString();
                String data=dataInJson();
                Boolean value=commonUtils.get_memory();
                if (!value.equals(true)) {

                    if (exist_id<100) {
                       smartMoveDatabaseAdapter.insertData(AppConstant.UNIQUE_ID, data, " ", " ", " ", " ", " ");

                }
                else {
                        smartMoveDatabaseAdapter.updateData(AppConstant.KEY_SELECT_PROJECT, data, exist_id);

                    }
                }
             commonUtils.set_memory_Storage(true);
                if (exist_id>99){
                    Bundle bundle=new Bundle();
                    bundle.putInt("fwd_id",exist_id);
                    SelectCategoryFragment selectCategoryFragment = new SelectCategoryFragment();
                    selectCategoryFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction().
                            add(R.id.container, selectCategoryFragment).addToBackStack(null).commit();
                }
                else {
                    SelectCategoryFragment selectCategoryFragment = new SelectCategoryFragment();
                    getFragmentManager().beginTransaction().
                            add(R.id.container, selectCategoryFragment).addToBackStack(null).commit();
                }

                break;

            case R.id.projectIV:
                if(flag==false){
                    megaprojectCV.setVisibility(View.GONE);
                    flag=true;
                }
                else {
                    megaprojectCV.setVisibility(View.VISIBLE);
                      flag=false;

                }

                break;
            case R.id.propertyareaIV:
                if(flag2==false){
                    propertyareaCV.setVisibility(View.INVISIBLE);
                    flag2=true;
                }
                else {
                    propertyareaCV.setVisibility(View.VISIBLE);
                    flag2=false;

                }
               break;
        }

    }
    public void editData(){
        String str_editable_data;
        Intent intent=getActivity().getIntent();
        exist_id = intent.getIntExtra("selected_id", 0);
            if (exist_id!=0)
            {
               str_editable_data=  smartMoveDatabaseAdapter.getSelectItems(exist_id, AppConstant.KEY_SELECT_PROJECT);
                Log.d("str_editable_data",""+str_editable_data);
                try {
                    JSONObject jsonObject=new JSONObject(str_editable_data);
                    JSONObject jsonObject1=jsonObject.optJSONObject("json2");
                    JSONObject jsonObject2=jsonObject.optJSONObject("json1");
                    String project=jsonObject2.optString("project");
                    String propertyarea=jsonObject1.optString("propertyarea");
                    if (megaprojectRB.getText().toString().equals(project))
                        megaprojectRB.setChecked(true);
                    if (resaleprojectRB.getText().toString().equals(project))
                        resaleprojectRB.setChecked(true);
                    if (rentprojectRB.getText().toString().equals(project))
                        rentprojectRB.setChecked(true);
                    if (commercialRB.getText().toString().equals(propertyarea))
                        commercialRB.setChecked(true);
                    if (propertareaRB.getText().toString().equals(propertyarea))
                        propertareaRB.setChecked(true);
                    if (flatRB.getText().toString().equals(propertyarea))
                        flatRB.setChecked(true);
                    if (industryRB.getText().toString().equals(propertyarea))
                        industryRB.setChecked(true);
                    if (kothiRB.getText().toString().equals(propertyarea))
                        kothiRB.setChecked(true);
                    if (plotRB.getText().toString().equals(propertyarea))
                        plotRB.setChecked(true);
                    if (otherRB.getText().toString().equals(propertyarea))
                        otherRB.setChecked(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
    }



    public String dataInJson(){
        JSONObject jsonObject=new JSONObject();
        JSONObject jsonObject1=new JSONObject();
        JSONObject jsonObject2=new JSONObject();

        try {
            jsonObject1.put("project",project_rb);
            jsonObject2.put("propertyarea",property_area_rb);
            jsonObject.put("json1",jsonObject1);
            jsonObject.put("json2",jsonObject2);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        object1=jsonObject.toString();
        Log.d("jso",""+jsonObject);
        return object1;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId==megaprojectRB.getId())
        {
            propertyareaCV1.setVisibility(View.INVISIBLE);
            propertyareaCV.setVisibility(View.INVISIBLE);
        }
        else {
            propertyareaCV1.setVisibility(View.VISIBLE);
            propertyareaCV.setVisibility(View.VISIBLE);
        }
    }
}
