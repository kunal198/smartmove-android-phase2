package com.smartmovereality.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.smartmovereality.Activities.MainActivity;
import com.smartmovereality.AppConstant;
import com.smartmovereality.CommonUtils;
import com.smartmovereality.R;
import com.smartmovereality.database.SmartMoveDatabaseAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 4/1/16.
 */
public class SelectCategoryFragment extends Fragment implements View.OnClickListener {

    ImageView nextIV,prevIV;
    CheckBox boothCB, commerciallandCB, foodcourtCB, godownCB, hospitalCB, hotelCB,
            indlandCB, officeCB, resortCB, farmhouseCB, flatCB, independenthouseCB, penthouseCB, residentialCB, studioCB, buyCB, rentCB, sellCB;

    JSONObject property_category_commercial_value = new JSONObject();
    JSONArray checkbox_selected=new JSONArray();
    JSONObject jsonObject = new JSONObject();
    CommonUtils commonUtils;
    String object2;
    SmartMoveDatabaseAdapter smartMoveDatabaseAdapter;
    int exists_id;
    ImageView propcatIV,proptypeIV;
CardView propertytypesCV,propertycategoryCV;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.select_category, container, false);
        init(view);
        commonUtils = new CommonUtils(getActivity());

        return view;
    }

    public void init(View view) {
        smartMoveDatabaseAdapter=new SmartMoveDatabaseAdapter(getActivity());
        nextIV = (ImageView) view.findViewById(R.id.nextIV);
        prevIV = (ImageView) view.findViewById(R.id.prevIV);
        nextIV.setOnClickListener(this);
        prevIV.setOnClickListener(this);
        boothCB = (CheckBox)view.findViewById(R.id.boothCB);
        commerciallandCB = (CheckBox) view.findViewById(R.id.commerciallandCB);
        foodcourtCB = (CheckBox) view.findViewById(R.id.foodcourtCB);
        godownCB = (CheckBox) view.findViewById(R.id.godownCB);
        hospitalCB = (CheckBox) view.findViewById(R.id.hospitalCB);
        hotelCB = (CheckBox) view.findViewById(R.id.hotelCB);
        indlandCB = (CheckBox) view.findViewById(R.id.indlandCB);
        officeCB = (CheckBox) view.findViewById(R.id.officeCB);
        resortCB = (CheckBox) view.findViewById(R.id.resortCB);
        farmhouseCB = (CheckBox) view.findViewById(R.id.farmhouseCB);
        flatCB = (CheckBox) view.findViewById(R.id.flatCB);
        independenthouseCB = (CheckBox) view.findViewById(R.id.independenthouseCB);
        penthouseCB = (CheckBox) view.findViewById(R.id.penthouseCB);
        residentialCB = (CheckBox) view.findViewById(R.id.residentialCB);
        studioCB = (CheckBox) view.findViewById(R.id.studioCB);
        buyCB = (CheckBox) view.findViewById(R.id.buyCB);
        rentCB = (CheckBox) view.findViewById(R.id.rentCB);
        sellCB = (CheckBox) view.findViewById(R.id.sellCB);
        propcatIV= (ImageView) view.findViewById(R.id.propcatIV);
        proptypeIV= (ImageView) view.findViewById(R.id.proptypeIV);
        propertytypesCV= (CardView) view.findViewById(R.id.propertytypesCV);
        propertycategoryCV= (CardView) view.findViewById(R.id.propertycategoryCV);
        propcatIV.setOnClickListener(this);
        proptypeIV.setOnClickListener(this);

        editData();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevIV:

                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.propcatIV:

                if (propertycategoryCV.getVisibility()==View.VISIBLE){

                    propertycategoryCV.setVisibility(View.GONE);
                }
                else {
                    propertycategoryCV.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.proptypeIV:
                if (propertytypesCV.getVisibility()==View.VISIBLE){

                    propertytypesCV.setVisibility(View.INVISIBLE);
                }
                else {
                    propertytypesCV.setVisibility(View.VISIBLE);
                }

                break;

            case R.id.nextIV:
                String data= checkBoxSelection();
                if (exists_id>99) {
                    smartMoveDatabaseAdapter.updateData(AppConstant.KEY_SELECT_CATEGORY, data,exists_id);
                }
                else {
                    smartMoveDatabaseAdapter.updateData(AppConstant.KEY_SELECT_CATEGORY, data, AppConstant.UNIQUE_ID);
                }
                if (exists_id>99){
                    Bundle bundle=new Bundle();
                    bundle.putInt("fwd_id", exists_id);
                    CheckAvailabilityFragment checkAvailabilityFragment = new CheckAvailabilityFragment();
                    checkAvailabilityFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction().
                            replace(R.id.container, checkAvailabilityFragment).addToBackStack(null).commit();
                }
                else {
                    CheckAvailabilityFragment checkAvailabilityFragment = new CheckAvailabilityFragment();
                    getFragmentManager().beginTransaction().
                            replace(R.id.container, checkAvailabilityFragment).addToBackStack(null).commit();
                }
                break;

        }
    }

    public String checkBoxSelection() {

            try {
                if (boothCB.isChecked()) {
                property_category_commercial_value.put("boothCB",(boothCB.getText().toString()));
            }
                if (commerciallandCB.isChecked()) {

                    property_category_commercial_value.put("commerciallandCB",(commerciallandCB.getText().toString()));
                }
                if (foodcourtCB.isChecked()) {

                    property_category_commercial_value.put("foodcourtCB",(foodcourtCB.getText().toString()));
                }
                if (godownCB.isChecked()) {
                    property_category_commercial_value.put("godownCB",(godownCB.getText().toString()));
                }
                if (hospitalCB.isChecked()) {
                    property_category_commercial_value.put("hospitalCB",(hospitalCB.getText().toString()));
                }
                if (hotelCB.isChecked()) {
                    property_category_commercial_value.put("hotelCB", (hotelCB.getText().toString()));
                }
                if (indlandCB.isChecked()) {
                    property_category_commercial_value.put("indlandCB",(indlandCB.getText().toString()));
                }
                if (officeCB.isChecked()) {
                    property_category_commercial_value.put("officeCB",(officeCB.getText().toString()));
                }
                if (resortCB.isChecked()) {
                    property_category_commercial_value.put("resortCB",(resortCB.getText().toString()));
                }
                if (farmhouseCB.isChecked()) {
                    property_category_commercial_value.put("farmhouseCB",(farmhouseCB.getText().toString()));
                }
                if (flatCB.isChecked()) {
                    property_category_commercial_value.put("flatCB",(flatCB.getText().toString()));
                }
                if (independenthouseCB.isChecked()) {
                    property_category_commercial_value.put("independenthouseCB",(independenthouseCB.getText().toString()));
                }
                if (penthouseCB.isChecked()) {
                    property_category_commercial_value.put("penthouseCB",(penthouseCB.getText().toString()));
                }
                if (residentialCB.isChecked()) {
                    property_category_commercial_value.put("residentialCB",(residentialCB.getText().toString()));
                }
                if (studioCB.isChecked()) {
                    property_category_commercial_value.put("studioCB",(studioCB.getText().toString()));
                }
                if (buyCB.isChecked()) {
                    property_category_commercial_value.put("buyCB",(buyCB.getText().toString()));
                }
                if (rentCB.isChecked()) {
                    property_category_commercial_value.put("rentCB",(rentCB.getText().toString()));
                }
                if (sellCB.isChecked()) {
                    property_category_commercial_value.put("sellCB",(sellCB.getText().toString()));
                }
            }catch (JSONException e) {
                e.printStackTrace();
            }

        checkbox_selected.put(property_category_commercial_value);
        try {
            jsonObject.put("checkbox_selected",checkbox_selected);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        object2=jsonObject.toString();
        Log.d("jsonnnn",""+jsonObject);
        return object2;
    }
    public void editData(){
        String str_editable_data;
        Bundle bundle=getArguments();
        if (bundle!=null)
        {
            exists_id=   bundle.getInt("fwd_id");
        }
        if (exists_id!=0)
        {
            str_editable_data=  smartMoveDatabaseAdapter.getSelectItems(exists_id, AppConstant.KEY_SELECT_CATEGORY);
            Log.d("str_editable_data",""+str_editable_data);
            try {
                JSONObject jsonObject = new JSONObject(str_editable_data);
                JSONArray jsonArray = jsonObject.optJSONArray("checkbox_selected");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                    String residential = jsonObject1.optString("residentialCB");
                    String buy = jsonObject1.optString("buyCB");
                    String office = jsonObject1.optString("officeCB");
                    String independenthouse = jsonObject1.optString("independenthouseCB");
                    String hospital = jsonObject1.optString("hospitalCB");
                    String penthouse = jsonObject1.optString("penthouseCB");
                    String sell = jsonObject1.optString("sellCB");
                    String farmhouse = jsonObject1.optString("farmhouseCB");
                    String flat = jsonObject1.optString("flatCB");
                    String godown = jsonObject1.optString("godownCB");
                    String studio = jsonObject1.optString("studioCB");
                    String commercialland = jsonObject1.optString("commerciallandCB");
                    String indland = jsonObject1.optString("indlandCB");
                    String booth = jsonObject1.optString("boothCB");
                    String rent = jsonObject1.optString("rentCB");
                    String hotel = jsonObject1.optString("hotelCB");
                    String foodcourt = jsonObject1.optString("foodcourtCB");
                    String resort=jsonObject1.optString("resortCB");
                    if (!residential.isEmpty())
                    residentialCB.setChecked(true);
                    if (!buy.isEmpty())
                    buyCB.setChecked(true);
                    if (!office.isEmpty())
                    officeCB.setChecked(true);
                    if (!independenthouse.isEmpty())
                    independenthouseCB.setChecked(true);
                    if (!hospital.isEmpty())
                    hospitalCB.setChecked(true);
                    if (!penthouse.isEmpty())
                    penthouseCB.setChecked(true);
                    if (!sell.isEmpty())
                    sellCB.setChecked(true);
                    if (!farmhouse.isEmpty())
                    farmhouseCB.setChecked(true);
                    if (!flat.isEmpty())
                    flatCB.setChecked(true);
                    if (!godown.isEmpty())
                    godownCB.setChecked(true);
                    if (!studio.isEmpty())
                    studioCB.setChecked(true);
                    if (!commercialland.isEmpty())
                    commerciallandCB.setChecked(true);
                    if (!indland.isEmpty())
                    indlandCB.setChecked(true);
                    if (!booth.isEmpty())
                    boothCB.setChecked(true);
                    if (!rent.isEmpty())
                    rentCB.setChecked(true);
                    if (!hotel.isEmpty())
                    hotelCB.setChecked(true);
                    if (!foodcourt.isEmpty())
                    foodcourtCB.setChecked(true);
                    if (!resort.isEmpty())
                        resortCB.setChecked(true);


                }
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }
    }

  /*  public String dataInJson() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();
        JSONArray jsonArray2 = new JSONArray();
        JSONArray jsonArray3 = new JSONArray();


        for (int i = 0; i < property_category_commercial_value.size(); i++) {

            try {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("property_category_commercial_value", property_category_commercial_value.get(i));
                jsonArray1.put(jsonObject1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < property_category_residential_value.size(); i++) {
            try {
                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put("property_category_residential_value", property_category_residential_value.get(i));
                jsonArray2.put(jsonObject2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < property_types_commercial_value.size(); i++) {

            try {
                JSONObject jsonObject3 = new JSONObject();
                jsonObject3.put("property_types_commercial_value", property_types_commercial_value.get(i));
                jsonArray3.put(jsonObject3);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            jsonObject.put("category_commercial_value", jsonArray1);
            jsonObject.put("category_residential_value", jsonArray2);
            jsonObject.put("types_commercial_value", jsonArray3);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        object2=jsonObject.toString();
        Log.d("jsonnnn",""+jsonObject);
        return object2;
    }*/
}